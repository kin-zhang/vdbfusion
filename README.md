# VDBFusion

原官方链接为：[https://github.com/PRBonn/vdbfusion](https://github.com/PRBonn/vdbfusion)

本repo主要给聪明进行 时间上的实验



## Install

原官方gcc版本规定有误，无法通过编译，不知道是不是我的Ubuntu版本问题，具体可见此博文：[https://blog.csdn.net/qq_39537898/article/details/125645474](https://blog.csdn.net/qq_39537898/article/details/125645474) 总结了相关的cmake问题

依赖就不重复说明了 在此 [kin_zhang/vdbfusion_mapping_ros](https://gitee.com/kin_zhang/vdbfusion_mapping/blob/master/assets/readme/install_desktop.md) 中基本是一致的，但是这个是可以无ros的，主要用于数据集的快速验证

```bash
git clone https://gitee.com/kin_zhang/vdbfusion
cd vdbfusion
mkdir build && cd build
cmake ..
make
```

docker 与ros那个可以通用
```bash
docker pull zhangkin/vdbmapping_mapping:kitti

# =========== RUN
docker run -it --net=host -v /dev/shm:/dev/shm --name vdbfusion_kitti zhangkin/vdbfusion_mapping:kitti /bin/zsh

# inside container 
cd .. && cd vdbfusion && git pull
cd build
cmake .. && make
./examples/cpp/kitti_pipeline "/workspace/data/KITTI_test" "/workspace/data" --n_scans 10 --config "/workspace/vdbfusion/examples/cpp/config/kitti.yaml"
```


## Running

此处需要下载kitti数据集，阿里云盘下载链接：[https://www.aliyundrive.com/s/AS2LXkYNyH3](https://www.aliyundrive.com/s/AS2LXkYNyH3) ??? zip为什么不能分享 这样的话 大家只能自己翻一下下载了 不然速度很慢；我下的是整体 然后单拎了一个seq 11作为测试

folder的结构需要是这样的

```bash
➜  KITTI pwd
/home/kin/Downloads/KITTI
➜  KITTI tree -L 3
.
├── poses
│   └── 11.txt
└── sequences
    └── 11
        ├── calib.txt
        └── velodyne

4 directories, 2 files
```

随后 运行和给出路径就行

```bash
# 如果你改好了config的话 就可以直接运行 默认是我的绝对路径
./examples/cpp/kitti_pipeline --n_scans 500
```

运行后结果：

可视化可以使用open3d python版

```python
import open3d as o3d
mesh = o3d.io.read_triangle_model('kitti_11_500_scans.ply')
o3d.visualization.draw(mesh)
```

图片示意：

![](docs/example.png)
